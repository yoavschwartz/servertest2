//
//  main.m
//  testOnlineGit3
//
//  Created by yoav schwartz on 11/5/13.
//  Copyright (c) 2013 iRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "testOnlineGit3AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([testOnlineGit3AppDelegate class]));
    }
}
